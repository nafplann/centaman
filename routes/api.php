<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
Route::get('/test', 'TestController@index');

Route::group(['prefix' => 'v1'], function() {
    Route::group(['prefix' => 'auth'], function() {
        Route::post('login', 'Auth\SessionsController@login');
        Route::post('signup', 'Auth\RegisterController@signup');
        
        Route::group(['middleware' => 'auth:api'], function() {
            Route::get('logout', 'Auth\SessionsController@logout');
            Route::get('user', 'Auth\SessionsController@user');
        });
    });

    // Route::group(['middleware' => 'auth:api'], function() {
        Route::get('/users', 'UsersController@index');
        Route::resource('/audit-history', 'AuditHistoryController@index');
        Route::resource('bookings', 'BookingsController');
        Route::resource('depthdr', 'DepthDrController');
        Route::resource('posemail', 'PosEmailController');
        Route::resource('credit-card-transactions', 'CreditCardTransactionController');
        Route::resource('ezi-debit-timestop-history', 'EziDebitTimestopHistoryController');
    // });
});

// Route::group(['prefix' => 'bookings'], function() {
//     Route::get('/', 'BookingsController@index');
//     Route::get('/{booking}', 'BookingsController@show');
//     Route::post('/{booking}', 'BookingsController@store');
//     Route::put('/{booking}', 'BookingsController@update');
//     Route::delete('/{booking}', 'BookingsController@destroy');
// });


