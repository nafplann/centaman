<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EziDebitTimestopHistory extends Model
{
    public $table = 'EziDebitTimestopHistory';
    public $timestamps = false;
    
    protected $primaryKey = 'EziDebitTimestopHistoryId';
    protected $fillable = ['EziDebitCustomerId', 'EziDebitTimestopId', 'ReactivateDate', 'ReactivateProcessed', 'StartDate', 'StartProcessed'];
}
