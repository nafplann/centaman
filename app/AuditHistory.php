<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AuditHistory extends Model
{
    public $table = 'audithistory';
}
