<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreBooking extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'bking_id' => 'required|integer',
            'bking_name' => 'required',
            'bkngtypeid' => 'required|integer',
            'priorityid' => 'required|integer',
            'confirmed',
            'confirm_by',
            'est_people',
            'bking_cont',
            'bking_date',
            'clintid',
            'rsrcs_cost',
            'xtras_cost',
            'rsrcs_tax',
            'xtras_tax',
            'discounts',
            'dscnt_info',
            'total_paid',
            'start_date',
            'start_time',
            'finsh_date',
            'finsh_time',
            'dep_amount',
            'dep_due_on',
            'dep_ispaid',
            'tot_due_on',
            'tot_ispaid',
            'inv_date',
            'inv_done',
            'bkng_notes',
            'bkg_status',
            'act_date',
            'notes',
            'usagecalc',
            'isprimary',
            'exclusive',
            'cancelled',
            'waitlist',
            'membercode',
            'GroupBooking',
            'AccessRightsTemplateId',
            'AttendStatus',
            'Pos_Cost',
            'Pos_Tax',
            'ExternalBookingReference',
            'PaymentToken'
        ];
    }
}
