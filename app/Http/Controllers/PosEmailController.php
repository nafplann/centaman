<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\PosEmail;

class PosEmailController extends Controller
{
    /**
     * @OA\Get(
     *      path="/api/v1/posemail",
     *      operationId="getPosEmailList",
     *      tags={"PosEmail"},
     *      summary="Get list of posemail",
     *      description="Returns list of posemail",
     *      security={{"passport": {"*"}}},
     *      @OA\Parameter(
     *          name="accept",
     *          description="Request should accept json type",
     *          required=true,
     *          example="application/json",
     *          in="header",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation"
     *       ),
     *       @OA\Response(response=400, description="Bad request"),
     *       security={
     *           {"api_key_security_example": {}}
     *       }
     *     )
     *
     * Returns list of posemail
     */
    public function index()
    {
        return response()->json(PosEmail::paginate(), 206);
    }

    /**
        * @OA\Post(
        *      path="/api/v1/posemail",
        *      operationId="createPosEmail",
        *      tags={"PosEmail"},
        *      summary="Create new posemail",
        *      description="Create new posemail",
        *      @OA\Parameter(
        *          name="centernumber",
        *          description="centernumber",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="integer",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="emailaddress",
        *          description="emailaddress",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="string",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="emailsource",
        *          description="emailsource",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="string",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="entityid",
        *          description="entityid",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="integer",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="nodenumber",
        *          description="nodenumber",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="integer",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="receiptamount",
        *          description="receiptamount",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="number",
        *              format="double"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="receiptdatetime",
        *          description="receiptdatetime",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="string",
        *              format="datetime"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="receiptnumber",
        *          description="receiptnumber",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="integer",
        *              format="-"
        *          )
        *      ),
        *      @OA\Response(response=201, description="Successful operation"),
        *      @OA\Response(response=400, description="Bad request"),
        *      @OA\Response(response=500, description="Internal server error"),
        *      security={
        *         {
        *             "oauth2_security_example": {"write:projects", "read:projects"}
        *         }
        *     },
        * )
    */
    public function store(Request $request)
    {
        try {
            $data = PosEmail::create($request->all());
        } catch (\Exception $e) {
            return response()->json([
                'error' => [
                    'message' => $e->getMessage()
                ]
            ], 500);
        }

        return response()->json($data, 201);
    }

    /**
     * @OA\Get(
     *      path="/api/v1/posemail/{autoid}",
     *      operationId="getPosEmailById",
     *      tags={"PosEmail"},
     *      summary="Get posemail information",
     *      description="Returns posemail data",
     *      @OA\Parameter(
     *          name="autoid",
     *          description="Unique id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="accept",
     *          description="Request should accept json type",
     *          required=true,
     *          example="application/json",
     *          in="header",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation"
     *       ),
     *      @OA\Response(response=400, description="Bad request"),
     *      @OA\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:projects", "read:projects"}
     *         }
     *     },
     * )
     */
    public function show(PosEmail $posemail)
    {
        return $posemail;
    }

    /**
        * @OA\Put(
        *      path="/api/v1/posemail/{autoid}",
        *      operationId="updatePosEmailById",
        *      tags={"PosEmail"},
        *      summary="Update posemail information",
        *      description="Returns posemail data",
        *      @OA\Parameter(
        *          name="autoid",
        *          description="autoid",
        *          required=true,
        *          in="path",
        *          @OA\Schema(
        *              type="integer"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="centernumber",
        *          description="centernumber",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="integer",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="emailaddress",
        *          description="emailaddress",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="string",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="emailsource",
        *          description="emailsource",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="string",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="entityid",
        *          description="entityid",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="integer",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="nodenumber",
        *          description="nodenumber",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="integer",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="receiptamount",
        *          description="receiptamount",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="number",
        *              format="double"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="receiptdatetime",
        *          description="receiptdatetime",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="string",
        *              format="datetime"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="receiptnumber",
        *          description="receiptnumber",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="integer",
        *              format="-"
        *          )
        *      ),
        *      @OA\Response(
        *          response=200,
        *          description="Successful operation"
        *       ),
        *      @OA\Response(response=400, description="Bad request"),
        *      @OA\Response(response=404, description="Resource Not Found"),
        *      security={
        *         {
        *             "oauth2_security_example": {"write:projects", "read:projects"}
        *         }
        *     },
        * )
    */
    public function update(Request $request, PosEmail $posemail)
    {
        try {
            $posemail->update($request->all());
        } catch (\Exception $e) {
            return response()->json([
                'error' => [
                    'message' => $e->getMessage()
                ]
            ], 500);
        }
        return response()->json($posemail, 200);
    }

    /**
     * @OA\Delete(
     *      path="/api/v1/posemail/{autoid}",
     *      operationId="removePosEmailById",
     *      tags={"PosEmail"},
     *      summary="Delete posemail",
     *      description="Returns posemail data",
     *      @OA\Parameter(
     *          name="autoid",
     *          description="Unique id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="accept",
     *          description="Request should accept json type",
     *          required=true,
     *          example="application/json",
     *          in="header",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Response(
     *          response=204,
     *          description="Successful operation"
     *       ),
     *      @OA\Response(response=400, description="Bad request"),
     *      @OA\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:projects", "read:projects"}
     *         }
     *     },
     * )
     */
    public function destroy(PosEmail $posemail)
    {
        $posemail->delete();
        return response()->json(null, 204);
    }
}
