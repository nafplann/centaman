<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\EziDebitTimestopHistory;

class EziDebitTimestopHistoryController extends Controller
{
    /**
     * @OA\Get(
     *      path="/api/v1/ezi-debit-timestop-history",
     *      operationId="getEziDebitTimestopHistoryList",
     *      tags={"EziDebitTimestopHistory"},
     *      summary="Get list of EziDebitTimestopHistory",
     *      description="Returns list of EziDebitTimestopHistory",
     *      security={{"passport": {"*"}}},
     *      @OA\Parameter(
     *          name="accept",
     *          description="Request should accept json type",
     *          required=true,
     *          example="application/json",
     *          in="header",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation"
     *       ),
     *       @OA\Response(response=400, description="Bad request"),
     *       security={
     *           {"api_key_security_example": {}}
     *       }
     *     )
     *
     * Returns list of EziDebitTimestopHistory
     */
    public function index()
    {
        return response()->json(EziDebitTimestopHistory::paginate(), 206);
    }

    /**
        * @OA\Post(
        *      path="/api/v1/ezi-debit-timestop-history",
        *      operationId="createEziDebitTimestopHistory",
        *      tags={"EziDebitTimestopHistory"},
        *      summary="Create new EziDebitTimestopHistory",
        *      description="Create new EziDebitTimestopHistory",
        *      @OA\Parameter(
        *          name="EziDebitCustomerId",
        *          description="EziDebitCustomerId",
        *          required=true,
        *          in="query",
        *          @OA\Schema(
        *              type="integer",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="EziDebitTimestopId",
        *          description="EziDebitTimestopId",
        *          required=true,
        *          in="query",
        *          @OA\Schema(
        *              type="integer",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="ReactivateDate",
        *          description="ReactivateDate",
        *          required=true,
        *          in="query",
        *          @OA\Schema(
        *              type="string",
        *              format="datetime"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="ReactivateProcessed",
        *          description="ReactivateProcessed",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="integer",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="StartDate",
        *          description="StartDate",
        *          required=true,
        *          in="query",
        *          @OA\Schema(
        *              type="string",
        *              format="datetime"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="StartProcessed",
        *          description="StartProcessed",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="integer",
        *              format="-"
        *          )
        *      ),
        *      @OA\Response(response=201, description="Successful operation"),
        *      @OA\Response(response=400, description="Bad request"),
        *      @OA\Response(response=500, description="Internal server error"),
        *      security={
        *         {
        *             "oauth2_security_example": {"write:projects", "read:projects"}
        *         }
        *     },
        * )
    */
    public function store(Request $request)
    {
        try {
            $data = EziDebitTimestopHistory::create($request->all());
        } catch (\Exception $e) {
            return response()->json([
                'error' => [
                    'message' => $e->getMessage()
                ]
            ], 500);
        }

        return response()->json($data, 201);
    }

    /**
     * @OA\Get(
     *      path="/api/v1/ezi-debit-timestop-history/{EziDebitTimestopHistoryId}",
     *      operationId="getEziDebitTimestopHistoryById",
     *      tags={"EziDebitTimestopHistory"},
     *      summary="Get EziDebitTimestopHistory information",
     *      description="Returns EziDebitTimestopHistory data",
     *      @OA\Parameter(
     *          name="EziDebitTimestopHistoryId",
     *          description="Unique id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="accept",
     *          description="Request should accept json type",
     *          required=true,
     *          example="application/json",
     *          in="header",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation"
     *       ),
     *      @OA\Response(response=400, description="Bad request"),
     *      @OA\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:projects", "read:projects"}
     *         }
     *     },
     * )
     */
    public function show(EziDebitTimestopHistory $eziDebitTimestopHistory)
    {
        return $eziDebitTimestopHistory;
    }

    /**
        * @OA\Put(
        *      path="/api/v1/ezi-debit-timestop-history/{EziDebitTimestopHistoryId}",
        *      operationId="updateEziDebitTimestopHistoryById",
        *      tags={"EziDebitTimestopHistory"},
        *      summary="Update EziDebitTimestopHistory information",
        *      description="Returns EziDebitTimestopHistory data",
        *      @OA\Parameter(
        *          name="EziDebitTimestopHistoryId",
        *          description="EziDebitTimestopHistoryId",
        *          required=true,
        *          in="path",
        *          @OA\Schema(
        *              type="integer",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="EziDebitCustomerId",
        *          description="EziDebitCustomerId",
        *          required=true,
        *          in="query",
        *          @OA\Schema(
        *              type="integer",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="EziDebitTimestopId",
        *          description="EziDebitTimestopId",
        *          required=true,
        *          in="query",
        *          @OA\Schema(
        *              type="integer",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="ReactivateDate",
        *          description="ReactivateDate",
        *          required=true,
        *          in="query",
        *          @OA\Schema(
        *              type="string",
        *              format="datetime"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="ReactivateProcessed",
        *          description="ReactivateProcessed",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="integer",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="StartDate",
        *          description="StartDate",
        *          required=true,
        *          in="query",
        *          @OA\Schema(
        *              type="string",
        *              format="datetime"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="StartProcessed",
        *          description="StartProcessed",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="integer",
        *              format="-"
        *          )
        *      ),
        *      @OA\Response(
        *          response=200,
        *          description="Successful operation"
        *       ),
        *      @OA\Response(response=400, description="Bad request"),
        *      @OA\Response(response=404, description="Resource Not Found"),
        *      security={
        *         {
        *             "oauth2_security_example": {"write:projects", "read:projects"}
        *         }
        *     },
        * )
    */
    public function update(Request $request, EziDebitTimestopHistory $eziDebitTimestopHistory)
    {
        try {
            $eziDebitTimestopHistory->update($request->all());
        } catch (\Exception $e) {
            return response()->json([
                'error' => [
                    'message' => $e->getMessage()
                ]
            ], 500);
        }
        return response()->json($eziDebitTimestopHistory, 200);
    }

    /**
     * @OA\Delete(
     *      path="/api/v1/ezi-debit-timestop-history/{EziDebitTimestopHistoryId}",
     *      operationId="removeEziDebitTimestopHistoryById",
     *      tags={"EziDebitTimestopHistory"},
     *      summary="Delete EziDebitTimestopHistory",
     *      description="Returns EziDebitTimestopHistory data",
     *      @OA\Parameter(
     *          name="EziDebitTimestopHistoryId",
     *          description="Unique id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="accept",
     *          description="Request should accept json type",
     *          required=true,
     *          example="application/json",
     *          in="header",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Response(
     *          response=204,
     *          description="Successful operation"
     *       ),
     *      @OA\Response(response=400, description="Bad request"),
     *      @OA\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:projects", "read:projects"}
     *         }
     *     },
     * )
     */
    public function destroy(EziDebitTimestopHistory $eziDebitTimestopHistory)
    {
        $eziDebitTimestopHistory->delete();
        return response()->json(null, 204);
    }
}
