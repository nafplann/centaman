<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\DepthDr;

class DepthDrController extends Controller
{
    /**
     * @OA\Get(
     *      path="/api/v1/depthdr",
     *      operationId="getDepthdrList",
     *      tags={"Depthdr"},
     *      summary="Get list of depthdr",
     *      description="Returns list of depthdr",
     *      security={{"passport": {"*"}}},
     *      @OA\Parameter(
     *          name="accept",
     *          description="Request should accept json type",
     *          required=true,
     *          example="application/json",
     *          in="header",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation"
     *       ),
     *       @OA\Response(response=400, description="Bad request"),
     *       security={
     *           {"api_key_security_example": {}}
     *       }
     *     )
     *
     * Returns list of depthdr
     */
    public function index()
    {
        return response()->json(DepthDr::paginate(), 206);
    }

    /**
        * @OA\Post(
        *      path="/api/v1/depthdr",
        *      operationId="createDepthdr",
        *      tags={"Depthdr"},
        *      summary="Create new depthdr",
        *      description="Create new depthdr",
        *      @OA\Parameter(
        *          name="UseExtendedAttributes",
        *          description="UseExtendedAttributes",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="integer",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="bpcxfile",
        *          description="bpcxfile",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="string",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="butt",
        *          description="butt",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="string",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="butt2",
        *          description="butt2",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="string",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="canc_from",
        *          description="canc_from",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="string",
        *              format="datetime"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="canc_till",
        *          description="canc_till",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="string",
        *              format="datetime"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="cancelled",
        *          description="cancelled",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="integer",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="cdesc",
        *          description="cdesc",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="string",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="certno",
        *          description="certno",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="string",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="code",
        *          description="code",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="string",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="crdate",
        *          description="crdate",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="string",
        *              format="datetime"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="crtime",
        *          description="crtime",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="string",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="date",
        *          description="date",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="string",
        *              format="datetime"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="dcode",
        *          description="dcode",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="integer",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="depttype",
        *          description="depttype",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="integer",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="dlevel",
        *          description="dlevel",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="integer",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="dow0",
        *          description="dow0",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="integer",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="dow1",
        *          description="dow1",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="integer",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="dow2",
        *          description="dow2",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="integer",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="dow3",
        *          description="dow3",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="integer",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="dow4",
        *          description="dow4",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="integer",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="dow5",
        *          description="dow5",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="integer",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="dow6",
        *          description="dow6",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="integer",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="hidedept",
        *          description="hidedept",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="integer",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="node",
        *          description="node",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="integer",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="rdesc",
        *          description="rdesc",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="string",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="repflag1",
        *          description="repflag1",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="integer",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="repflag2",
        *          description="repflag2",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="integer",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="retain",
        *          description="retain",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="integer",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="sdate",
        *          description="sdate",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="string",
        *              format="datetime"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="snum",
        *          description="snum",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="integer",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="stime",
        *          description="stime",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="string",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="time",
        *          description="time",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="string",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="ucode",
        *          description="ucode",
        *          required=true,
        *          in="query",
        *          @OA\Schema(
        *              type="integer",
        *              format="-"
        *          )
        *      ),
        *      @OA\Response(response=201, description="Successful operation"),
        *      @OA\Response(response=400, description="Bad request"),
        *      @OA\Response(response=500, description="Internal server error"),
        *      security={
        *         {
        *             "oauth2_security_example": {"write:projects", "read:projects"}
        *         }
        *     },
        * )
    */
    public function store(Request $request)
    {
        try {
            $data = Depthdr::create($request->all());
        } catch (\Exception $e) {
            return response()->json([
                'error' => [
                    'message' => $e->getMessage()
                ]
            ], 500);
        }

        return response()->json($data, 201);
    }

    /**
     * @OA\Get(
     *      path="/api/v1/depthdr/{autoid}",
     *      operationId="getDepthdrById",
     *      tags={"Depthdr"},
     *      summary="Get depthdr information",
     *      description="Returns depthdr data",
     *      @OA\Parameter(
     *          name="autoid",
     *          description="Unique id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="accept",
     *          description="Request should accept json type",
     *          required=true,
     *          example="application/json",
     *          in="header",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation"
     *       ),
     *      @OA\Response(response=400, description="Bad request"),
     *      @OA\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:projects", "read:projects"}
     *         }
     *     },
     * )
     */
    public function show(Depthdr $depthdr)
    {
        return $depthdr;
    }

    /**
        * @OA\Put(
        *      path="/api/v1/depthdr/{autoid}",
        *      operationId="updateDepthdrById",
        *      tags={"Depthdr"},
        *      summary="Update depthdr information",
        *      description="Returns depthdr data",
        *      @OA\Parameter(
        *          name="autoid",
        *          description="autoid",
        *          required=true,
        *          in="path",
        *          @OA\Schema(
        *              type="integer"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="UseExtendedAttributes",
        *          description="UseExtendedAttributes",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="integer",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="bpcxfile",
        *          description="bpcxfile",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="string",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="butt",
        *          description="butt",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="string",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="butt2",
        *          description="butt2",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="string",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="canc_from",
        *          description="canc_from",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="string",
        *              format="datetime"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="canc_till",
        *          description="canc_till",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="string",
        *              format="datetime"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="cancelled",
        *          description="cancelled",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="integer",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="cdesc",
        *          description="cdesc",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="string",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="certno",
        *          description="certno",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="string",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="code",
        *          description="code",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="string",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="crdate",
        *          description="crdate",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="string",
        *              format="datetime"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="crtime",
        *          description="crtime",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="string",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="date",
        *          description="date",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="string",
        *              format="datetime"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="dcode",
        *          description="dcode",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="integer",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="depttype",
        *          description="depttype",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="integer",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="dlevel",
        *          description="dlevel",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="integer",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="dow0",
        *          description="dow0",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="integer",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="dow1",
        *          description="dow1",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="integer",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="dow2",
        *          description="dow2",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="integer",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="dow3",
        *          description="dow3",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="integer",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="dow4",
        *          description="dow4",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="integer",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="dow5",
        *          description="dow5",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="integer",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="dow6",
        *          description="dow6",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="integer",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="hidedept",
        *          description="hidedept",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="integer",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="node",
        *          description="node",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="integer",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="rdesc",
        *          description="rdesc",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="string",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="repflag1",
        *          description="repflag1",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="integer",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="repflag2",
        *          description="repflag2",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="integer",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="retain",
        *          description="retain",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="integer",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="sdate",
        *          description="sdate",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="string",
        *              format="datetime"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="snum",
        *          description="snum",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="integer",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="stime",
        *          description="stime",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="string",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="time",
        *          description="time",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="string",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="ucode",
        *          description="ucode",
        *          required=true,
        *          in="query",
        *          @OA\Schema(
        *              type="integer",
        *              format="-"
        *          )
        *      ),
        *      @OA\Response(
        *          response=200,
        *          description="Successful operation"
        *       ),
        *      @OA\Response(response=400, description="Bad request"),
        *      @OA\Response(response=404, description="Resource Not Found"),
        *      security={
        *         {
        *             "oauth2_security_example": {"write:projects", "read:projects"}
        *         }
        *     },
        * )
    */
    public function update(Request $request, Depthdr $depthdr)
    {
        try {
            $depthdr->update($request->all());
        } catch (\Exception $e) {
            return response()->json([
                'error' => [
                    'message' => $e->getMessage()
                ]
            ], 500);
        }
        return response()->json($depthdr, 200);
    }

    /**
     * @OA\Delete(
     *      path="/api/v1/depthdr/{autoid}",
     *      operationId="removeDepthdrById",
     *      tags={"Depthdr"},
     *      summary="Delete depthdr",
     *      description="Returns depthdr data",
     *      @OA\Parameter(
     *          name="autoid",
     *          description="Unique id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="accept",
     *          description="Request should accept json type",
     *          required=true,
     *          example="application/json",
     *          in="header",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Response(
     *          response=204,
     *          description="Successful operation"
     *       ),
     *      @OA\Response(response=400, description="Bad request"),
     *      @OA\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:projects", "read:projects"}
     *         }
     *     },
     * )
     */
    public function destroy(Depthdr $depthdr)
    {
        $depthdr->delete();
        return response()->json(null, 204);
    }
}
