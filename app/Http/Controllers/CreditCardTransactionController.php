<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\CreditCardTransaction;

class CreditCardTransactionController extends Controller
{
    /**
     * @OA\Get(
     *      path="/api/v1/credit-card-transactions",
     *      operationId="getCreditCardTransactionList",
     *      tags={"CreditCardTransaction"},
     *      summary="Get list of CreditCardTransaction",
     *      description="Returns list of CreditCardTransaction",
     *      security={{"passport": {"*"}}},
     *      @OA\Parameter(
     *          name="accept",
     *          description="Request should accept json type",
     *          required=true,
     *          example="application/json",
     *          in="header",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation"
     *       ),
     *       @OA\Response(response=400, description="Bad request"),
     *       security={
     *           {"api_key_security_example": {}}
     *       }
     *     )
     *
     * Returns list of CreditCardTransaction
     */
    public function index()
    {
        return response()->json(CreditCardTransaction::paginate(), 206);
    }

    /**
        * @OA\Post(
        *      path="/api/v1/credit-card-transactions",
        *      operationId="createCreditCardTransaction",
        *      tags={"CreditCardTransaction"},
        *      summary="Create new CreditCardTransaction",
        *      description="Create new CreditCardTransaction",
        *      @OA\Parameter(
        *          name="AccountType",
        *          description="AccountType",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="integer",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="AccountingDateTime",
        *          description="AccountingDateTime",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="string",
        *              format="datetime"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="AcqRefData",
        *          description="AcqRefData",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="string",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="AuthCode",
        *          description="AuthCode",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="string",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="AuthorizeAmount",
        *          description="AuthorizeAmount",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="integer",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="CardType",
        *          description="CardType",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="string",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="CashOut",
        *          description="CashOut",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="integer",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="CmdStatus",
        *          description="CmdStatus",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="string",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="CreditProcessor",
        *          description="CreditProcessor",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="string",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="CreditProcessorSubType",
        *          description="CreditProcessorSubType",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="string",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="EntryMethod",
        *          description="EntryMethod",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="string",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="InvoiceNo",
        *          description="InvoiceNo",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="string",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="MerchantID",
        *          description="MerchantID",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="string",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="NodeNo",
        *          description="NodeNo",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="integer",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="OperatorID",
        *          description="OperatorID",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="string",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="ReceiptNo",
        *          description="ReceiptNo",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="integer",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="ReceiptText",
        *          description="ReceiptText",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="string",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="RecordNo",
        *          description="RecordNo",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="string",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="RefNo",
        *          description="RefNo",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="string",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="RequestedPurchaseAmount",
        *          description="RequestedPurchaseAmount",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="integer",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="STAN",
        *          description="STAN",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="integer",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="SignMaximumX",
        *          description="SignMaximumX",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="integer",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="SignMaximumY",
        *          description="SignMaximumY",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="integer",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="Signature",
        *          description="Signature",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="string",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="Source",
        *          description="Source",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="string",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="TerminalID",
        *          description="TerminalID",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="string",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="TextResponse",
        *          description="TextResponse",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="string",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="TipAmount",
        *          description="TipAmount",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="integer",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="TranCode",
        *          description="TranCode",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="string",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="TransDateTime",
        *          description="TransDateTime",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="string",
        *              format="datetime"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="TransNo",
        *          description="TransNo",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="integer",
        *              format="-"
        *          )
        *      ),
        *      @OA\Response(response=201, description="Successful operation"),
        *      @OA\Response(response=400, description="Bad request"),
        *      @OA\Response(response=500, description="Internal server error"),
        *      security={
        *         {
        *             "oauth2_security_example": {"write:projects", "read:projects"}
        *         }
        *     },
        * )
    */
    public function store(Request $request)
    {
        try {
            $data = CreditCardTransaction::create($request->all());
        } catch (\Exception $e) {
            return response()->json([
                'error' => [
                    'message' => $e->getMessage()
                ]
            ], 500);
        }

        return response()->json($data, 201);
    }

    /**
     * @OA\Get(
     *      path="/api/v1/credit-card-transactions/{CCTID}",
     *      operationId="getCreditCardTransactionById",
     *      tags={"CreditCardTransaction"},
     *      summary="Get CreditCardTransaction information",
     *      description="Returns CreditCardTransaction data",
     *      @OA\Parameter(
     *          name="CCTID",
     *          description="Unique id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="accept",
     *          description="Request should accept json type",
     *          required=true,
     *          example="application/json",
     *          in="header",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation"
     *       ),
     *      @OA\Response(response=400, description="Bad request"),
     *      @OA\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:projects", "read:projects"}
     *         }
     *     },
     * )
     */
    public function show(CreditCardTransaction $creditCardTransaction)
    {
        return $creditCardTransaction;
    }

    /**
        * @OA\Put(
        *      path="/api/v1/credit-card-transactions/{CCTID}",
        *      operationId="updateCreditCardTransactionById",
        *      tags={"CreditCardTransaction"},
        *      summary="Update CreditCardTransaction information",
        *      description="Returns CreditCardTransaction data",
        *      @OA\Parameter(
        *          name="CCTID",
        *          description="CCTID",
        *          required=true,
        *          in="path",
        *          @OA\Schema(
        *              type="integer"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="AccountType",
        *          description="AccountType",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="integer",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="AccountingDateTime",
        *          description="AccountingDateTime",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="string",
        *              format="datetime"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="AcqRefData",
        *          description="AcqRefData",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="string",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="AuthCode",
        *          description="AuthCode",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="string",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="AuthorizeAmount",
        *          description="AuthorizeAmount",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="integer",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="CardType",
        *          description="CardType",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="string",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="CashOut",
        *          description="CashOut",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="integer",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="CmdStatus",
        *          description="CmdStatus",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="string",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="CreditProcessor",
        *          description="CreditProcessor",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="string",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="CreditProcessorSubType",
        *          description="CreditProcessorSubType",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="string",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="EntryMethod",
        *          description="EntryMethod",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="string",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="InvoiceNo",
        *          description="InvoiceNo",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="string",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="MerchantID",
        *          description="MerchantID",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="string",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="NodeNo",
        *          description="NodeNo",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="integer",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="OperatorID",
        *          description="OperatorID",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="string",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="ReceiptNo",
        *          description="ReceiptNo",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="integer",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="ReceiptText",
        *          description="ReceiptText",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="string",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="RecordNo",
        *          description="RecordNo",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="string",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="RefNo",
        *          description="RefNo",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="string",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="RequestedPurchaseAmount",
        *          description="RequestedPurchaseAmount",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="integer",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="STAN",
        *          description="STAN",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="integer",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="SignMaximumX",
        *          description="SignMaximumX",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="integer",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="SignMaximumY",
        *          description="SignMaximumY",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="integer",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="Signature",
        *          description="Signature",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="string",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="Source",
        *          description="Source",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="string",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="TerminalID",
        *          description="TerminalID",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="string",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="TextResponse",
        *          description="TextResponse",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="string",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="TipAmount",
        *          description="TipAmount",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="integer",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="TranCode",
        *          description="TranCode",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="string",
        *              format="-"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="TransDateTime",
        *          description="TransDateTime",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="string",
        *              format="datetime"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="TransNo",
        *          description="TransNo",
        *          required=false,
        *          in="query",
        *          @OA\Schema(
        *              type="integer",
        *              format="-"
        *          )
        *      ),
        *      @OA\Response(
        *          response=200,
        *          description="Successful operation"
        *       ),
        *      @OA\Response(response=400, description="Bad request"),
        *      @OA\Response(response=404, description="Resource Not Found"),
        *      security={
        *         {
        *             "oauth2_security_example": {"write:projects", "read:projects"}
        *         }
        *     },
        * )
    */
    public function update(Request $request, CreditCardTransaction $creditCardTransaction)
    {
        try {
            $creditCardTransaction->update($request->all());
        } catch (\Exception $e) {
            return response()->json([
                'error' => [
                    'message' => $e->getMessage()
                ]
            ], 500);
        }
        return response()->json($creditCardTransaction, 200);
    }

    /**
     * @OA\Delete(
     *      path="/api/v1/credit-card-transactions/{CCTID}",
     *      operationId="removeCreditCardTransactionById",
     *      tags={"CreditCardTransaction"},
     *      summary="Delete CreditCardTransaction",
     *      description="Returns CreditCardTransaction data",
     *      @OA\Parameter(
     *          name="CCTID",
     *          description="Unique id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="accept",
     *          description="Request should accept json type",
     *          required=true,
     *          example="application/json",
     *          in="header",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Response(
     *          response=204,
     *          description="Successful operation"
     *       ),
     *      @OA\Response(response=400, description="Bad request"),
     *      @OA\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:projects", "read:projects"}
     *         }
     *     },
     * )
     */
    public function destroy(CreditCardTransaction $creditCardTransaction)
    {
        $creditCardTransaction->delete();
        return response()->json(null, 204);
    }
}
