<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Booking;

class BookingsController extends Controller
{
    /**
     * @OA\Get(
     *      path="/api/v1/bookings",
     *      operationId="getBookingsList",
     *      tags={"Bookings"},
     *      summary="Get list of bookings",
     *      description="Returns list of bookings",
     *      security={{"passport": {"*"}}},
     *      @OA\Parameter(
     *          name="accept",
     *          description="Request should accept json type",
     *          required=true,
     *          example="application/json",
     *          in="header",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation"
     *       ),
     *       @OA\Response(response=400, description="Bad request"),
     *       security={
     *           {"api_key_security_example": {}}
     *       }
     *     )
     *
     * Returns list of bookings
     */
    public function index()
    {
        return response()->json(Booking::paginate(), 206);
    }

    /**
        * @OA\Post(
        *      path="/api/v1/bookings",
        *      operationId="createBooking",
        *      tags={"Bookings"},
        *      summary="Create new booking",
        *      description="Create new booking",
        *      @OA\Parameter(
        *          name="bking_id",
        *          description="bking_id",
        *          required=true,
        *          in="query",
        *          @OA\Schema(
        *              type="integer"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="bking_name",
        *          description="bking_name",
        *          required=true,
        *          in="query",
        *          @OA\Schema(
        *              type="string"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="bkngtypeid",
        *          description="bkngtypeid",
        *          required=true,
        *          in="query",
        *          @OA\Schema(
        *              type="integer"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="priorityid",
        *          description="priorityid",
        *          required=true,
        *          in="query",
        *          @OA\Schema(
        *              type="integer"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="confirmed",
        *          description="confirmed",
        *          in="query",
        *          @OA\Schema(
        *              type="string"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="confirm_by",
        *          description="confirm_by",
        *          in="query",
        *          @OA\Schema(
        *              type="string"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="est_people",
        *          description="est_people",
        *          in="query",
        *          @OA\Schema(
        *              type="string"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="bking_cont",
        *          description="bking_cont",
        *          in="query",
        *          @OA\Schema(
        *              type="integer"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="bking_date",
        *          description="bking_date",
        *          in="query",
        *          @OA\Schema(
        *              type="string"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="clintid",
        *          description="clintid",
        *          in="query",
        *          @OA\Schema(
        *              type="integer"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="rsrcs_cost",
        *          description="rsrcs_cost",
        *          in="query",
        *          @OA\Schema(
        *              type="integer"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="xtras_cost",
        *          description="xtras_cost",
        *          in="query",
        *          @OA\Schema(
        *              type="integer"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="rsrcs_tax",
        *          description="rsrcs_tax",
        *          in="query",
        *          @OA\Schema(
        *              type="integer"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="xtras_tax",
        *          description="xtras_tax",
        *          in="query",
        *          @OA\Schema(
        *              type="integer"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="discounts",
        *          description="discounts",
        *          in="query",
        *          @OA\Schema(
        *              type="integer"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="dscnt_info",
        *          description="dscnt_info",
        *          in="query",
        *          @OA\Schema(
        *              type="string"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="total_paid",
        *          description="total_paid",
        *          in="query",
        *          @OA\Schema(
        *              type="integer"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="start_date",
        *          description="start_date",
        *          in="query",
        *          @OA\Schema(
        *              type="string"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="start_time",
        *          description="start_time",
        *          in="query",
        *          @OA\Schema(
        *              type="string"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="finsh_date",
        *          description="finsh_date",
        *          in="query",
        *          @OA\Schema(
        *              type="string"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="finsh_time",
        *          description="finsh_time",
        *          in="query",
        *          @OA\Schema(
        *              type="string"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="dep_amount",
        *          description="dep_amount",
        *          in="query",
        *          @OA\Schema(
        *              type="integer"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="dep_due_on",
        *          description="dep_due_on",
        *          in="query",
        *          @OA\Schema(
        *              type="string"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="dep_ispaid",
        *          description="dep_ispaid",
        *          in="query",
        *          @OA\Schema(
        *              type="integer"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="tot_due_on",
        *          description="tot_due_on",
        *          in="query",
        *          @OA\Schema(
        *              type="string"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="tot_ispaid",
        *          description="tot_ispaid",
        *          in="query",
        *          @OA\Schema(
        *              type="string"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="inv_date",
        *          description="inv_date",
        *          in="query",
        *          @OA\Schema(
        *              type="string"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="inv_done",
        *          description="inv_done",
        *          in="query",
        *          @OA\Schema(
        *              type="integer"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="bkng_notes",
        *          description="bkng_notes",
        *          in="query",
        *          @OA\Schema(
        *              type="string"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="bkg_status",
        *          description="bkg_status",
        *          in="query",
        *          @OA\Schema(
        *              type="string"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="act_date",
        *          description="act_date",
        *          in="query",
        *          @OA\Schema(
        *              type="string"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="notes",
        *          description="notes",
        *          in="query",
        *          @OA\Schema(
        *              type="string"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="usagecalc",
        *          description="usagecalc",
        *          in="query",
        *          @OA\Schema(
        *              type="integer"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="isprimary",
        *          description="isprimary",
        *          in="query",
        *          @OA\Schema(
        *              type="integer"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="exclusive",
        *          description="exclusive",
        *          in="query",
        *          @OA\Schema(
        *              type="string"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="cancelled",
        *          description="cancelled",
        *          in="query",
        *          @OA\Schema(
        *              type="string"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="waitlist",
        *          description="waitlist",
        *          in="query",
        *          @OA\Schema(
        *              type="string"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="membercode",
        *          description="membercode",
        *          in="query",
        *          @OA\Schema(
        *              type="string"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="GroupBooking",
        *          description="GroupBooking",
        *          in="query",
        *          @OA\Schema(
        *              type="string"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="AccessRightsTemplateId",
        *          description="AccessRightsTemplateId",
        *          in="query",
        *          @OA\Schema(
        *              type="integer"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="AttendStatus",
        *          description="AttendStatus",
        *          in="query",
        *          @OA\Schema(
        *              type="string"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="Pos_Cost",
        *          description="Pos_Cost",
        *          in="query",
        *          @OA\Schema(
        *              type="integer"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="Pos_Tax",
        *          description="Pos_Tax",
        *          in="query",
        *          @OA\Schema(
        *              type="integer"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="ExternalBookingReference",
        *          description="ExternalBookingReference",
        *          in="query",
        *          @OA\Schema(
        *              type="integer"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="PaymentToken",
        *          description="PaymentToken",
        *          in="query",
        *          @OA\Schema(
        *              type="string"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="accept",
        *          description="Request should accept json type",
        *          required=true,
        *          example="application/json",
        *          in="header",
        *          @OA\Schema(
        *              type="string"
        *          )
        *      ),
        *      @OA\Response(response=201, description="Successful operation"),
        *      @OA\Response(response=400, description="Bad request"),
        *      @OA\Response(response=500, description="Internal server error"),
        *      security={
        *         {
        *             "oauth2_security_example": {"write:projects", "read:projects"}
        *         }
        *     },
        * )
    */
    public function store(Request $request)
    {
        try {
            $booking = Booking::create($request->all());
        } catch (\Exception $e) {
            return response()->json([
                'error' => [
                    'message' => $e->getMessage()
                ]
            ], 500);
        }

        return response()->json($booking, 201);
    }

    /**
     * @OA\Get(
     *      path="/api/v1/bookings/{autoid}",
     *      operationId="getBookingById",
     *      tags={"Bookings"},
     *      summary="Get booking information",
     *      description="Returns booking data",
     *      @OA\Parameter(
     *          name="autoid",
     *          description="Unique id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="accept",
     *          description="Request should accept json type",
     *          required=true,
     *          example="application/json",
     *          in="header",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation"
     *       ),
     *      @OA\Response(response=400, description="Bad request"),
     *      @OA\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:projects", "read:projects"}
     *         }
     *     },
     * )
     */
    public function show(Booking $booking)
    {
        return $booking;
    }

    /**
        * @OA\Put(
        *      path="/api/v1/bookings/{autoid}",
        *      operationId="updateBookingById",
        *      tags={"Bookings"},
        *      summary="Update booking information",
        *      description="Returns booking data",
        *      @OA\Parameter(
        *          name="bking_id",
        *          description="bking_id",
        *          required=true,
        *          in="query",
        *          @OA\Schema(
        *              type="integer"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="bking_name",
        *          description="bking_name",
        *          required=true,
        *          in="query",
        *          @OA\Schema(
        *              type="string"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="bkngtypeid",
        *          description="bkngtypeid",
        *          required=true,
        *          in="query",
        *          @OA\Schema(
        *              type="integer"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="priorityid",
        *          description="priorityid",
        *          required=true,
        *          in="query",
        *          @OA\Schema(
        *              type="integer"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="confirmed",
        *          description="confirmed",
        *          in="query",
        *          @OA\Schema(
        *              type="string"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="confirm_by",
        *          description="confirm_by",
        *          in="query",
        *          @OA\Schema(
        *              type="string"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="est_people",
        *          description="est_people",
        *          in="query",
        *          @OA\Schema(
        *              type="string"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="bking_cont",
        *          description="bking_cont",
        *          in="query",
        *          @OA\Schema(
        *              type="integer"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="bking_date",
        *          description="bking_date",
        *          in="query",
        *          @OA\Schema(
        *              type="string"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="clintid",
        *          description="clintid",
        *          in="query",
        *          @OA\Schema(
        *              type="integer"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="rsrcs_cost",
        *          description="rsrcs_cost",
        *          in="query",
        *          @OA\Schema(
        *              type="integer"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="xtras_cost",
        *          description="xtras_cost",
        *          in="query",
        *          @OA\Schema(
        *              type="integer"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="rsrcs_tax",
        *          description="rsrcs_tax",
        *          in="query",
        *          @OA\Schema(
        *              type="integer"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="xtras_tax",
        *          description="xtras_tax",
        *          in="query",
        *          @OA\Schema(
        *              type="integer"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="discounts",
        *          description="discounts",
        *          in="query",
        *          @OA\Schema(
        *              type="integer"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="dscnt_info",
        *          description="dscnt_info",
        *          in="query",
        *          @OA\Schema(
        *              type="string"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="total_paid",
        *          description="total_paid",
        *          in="query",
        *          @OA\Schema(
        *              type="integer"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="start_date",
        *          description="start_date",
        *          in="query",
        *          @OA\Schema(
        *              type="string"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="start_time",
        *          description="start_time",
        *          in="query",
        *          @OA\Schema(
        *              type="string"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="finsh_date",
        *          description="finsh_date",
        *          in="query",
        *          @OA\Schema(
        *              type="string"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="finsh_time",
        *          description="finsh_time",
        *          in="query",
        *          @OA\Schema(
        *              type="string"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="dep_amount",
        *          description="dep_amount",
        *          in="query",
        *          @OA\Schema(
        *              type="integer"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="dep_due_on",
        *          description="dep_due_on",
        *          in="query",
        *          @OA\Schema(
        *              type="string"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="dep_ispaid",
        *          description="dep_ispaid",
        *          in="query",
        *          @OA\Schema(
        *              type="integer"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="tot_due_on",
        *          description="tot_due_on",
        *          in="query",
        *          @OA\Schema(
        *              type="string"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="tot_ispaid",
        *          description="tot_ispaid",
        *          in="query",
        *          @OA\Schema(
        *              type="string"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="inv_date",
        *          description="inv_date",
        *          in="query",
        *          @OA\Schema(
        *              type="string"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="inv_done",
        *          description="inv_done",
        *          in="query",
        *          @OA\Schema(
        *              type="integer"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="bkng_notes",
        *          description="bkng_notes",
        *          in="query",
        *          @OA\Schema(
        *              type="string"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="bkg_status",
        *          description="bkg_status",
        *          in="query",
        *          @OA\Schema(
        *              type="string"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="act_date",
        *          description="act_date",
        *          in="query",
        *          @OA\Schema(
        *              type="string"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="notes",
        *          description="notes",
        *          in="query",
        *          @OA\Schema(
        *              type="string"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="usagecalc",
        *          description="usagecalc",
        *          in="query",
        *          @OA\Schema(
        *              type="integer"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="isprimary",
        *          description="isprimary",
        *          in="query",
        *          @OA\Schema(
        *              type="integer"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="exclusive",
        *          description="exclusive",
        *          in="query",
        *          @OA\Schema(
        *              type="string"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="cancelled",
        *          description="cancelled",
        *          in="query",
        *          @OA\Schema(
        *              type="string"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="waitlist",
        *          description="waitlist",
        *          in="query",
        *          @OA\Schema(
        *              type="string"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="membercode",
        *          description="membercode",
        *          in="query",
        *          @OA\Schema(
        *              type="string"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="GroupBooking",
        *          description="GroupBooking",
        *          in="query",
        *          @OA\Schema(
        *              type="string"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="AccessRightsTemplateId",
        *          description="AccessRightsTemplateId",
        *          in="query",
        *          @OA\Schema(
        *              type="integer"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="AttendStatus",
        *          description="AttendStatus",
        *          in="query",
        *          @OA\Schema(
        *              type="string"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="Pos_Cost",
        *          description="Pos_Cost",
        *          in="query",
        *          @OA\Schema(
        *              type="integer"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="Pos_Tax",
        *          description="Pos_Tax",
        *          in="query",
        *          @OA\Schema(
        *              type="integer"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="ExternalBookingReference",
        *          description="ExternalBookingReference",
        *          in="query",
        *          @OA\Schema(
        *              type="integer"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="PaymentToken",
        *          description="PaymentToken",
        *          in="query",
        *          @OA\Schema(
        *              type="string"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="accept",
        *          description="Request should accept json type",
        *          required=true,
        *          example="application/json",
        *          in="header",
        *          @OA\Schema(
        *              type="string"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="accept",
        *          description="Request should accept json type",
        *          required=true,
        *          example="application/json",
        *          in="header",
        *          @OA\Schema(
        *              type="string"
        *          )
        *      ),
        *      @OA\Response(
        *          response=200,
        *          description="Successful operation"
        *       ),
        *      @OA\Response(response=400, description="Bad request"),
        *      @OA\Response(response=404, description="Resource Not Found"),
        *      security={
        *         {
        *             "oauth2_security_example": {"write:projects", "read:projects"}
        *         }
        *     },
        * )
    */
    public function update(Request $request, Booking $booking)
    {
        $booking->update($request->all());
        return response()->json($booking, 200);
    }

    /**
     * @OA\Delete(
     *      path="/api/v1/bookings/{autoid}",
     *      operationId="removeBookingById",
     *      tags={"Bookings"},
     *      summary="Delete booking",
     *      description="Returns booking data",
     *      @OA\Parameter(
     *          name="autoid",
     *          description="Unique id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="accept",
     *          description="Request should accept json type",
     *          required=true,
     *          example="application/json",
     *          in="header",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation"
     *       ),
     *      @OA\Response(response=400, description="Bad request"),
     *      @OA\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:projects", "read:projects"}
     *         }
     *     },
     * )
     */
    public function destroy(Booking $booking)
    {
        $booking->delete();
        return response()->json(null, 204);
    }
}
