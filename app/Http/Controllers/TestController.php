<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TestController extends Controller
{
    public function index()
    {
        // OpenAPI Data Types
        // string (this includes dates and files)
        // number
        // integer
        // boolean
        // array
        // object

        $sqlsrvDictionary = [
            'bigint' => [
                'type' => 'integer',
                'format' => '-'
            ],
            'bit' => [
                'type' => 'integer',
                'format' => '-'
            ],
            'decimal' => [
                'type' => 'number',
                'format' => 'double'
            ],
            'float' => [
                'type' => 'number',
                'format' => 'float'
            ],
            'int' => [
                'type' => 'integer',
                'format' => '-'
            ],
            'money' => [
                'type' => 'integer',
                'format' => '-'
            ],
            'date' => [
                'type' => 'string',
                'format' => 'date'
            ],
            'datetime' => [
                'type' => 'string',
                'format' => 'datetime'
            ],
            'char' => [
                'type' => 'string',
                'format' => '-'
            ],
            'text' => [
                'type' => 'integer',
                'format' => '-'
            ],
            'nchar' => [
                'type' => 'string',
                'format' => '-'
            ],
            'nvarchar' => [
                'type' => 'string',
                'format' => '-'
            ],
            'varchar' => [
                'type' => 'string',
                'format' => '-'
            ],
            'ntext' => [
                'type' => 'string',
                'format' => '-'
            ]
        ];

        // $tables = DB::select("SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE'");
      
        // foreach ($tables as $table) {
        //     // dd($table);
        //     echo $table->TABLE_NAME;
        //     echo '<hr>';
        // }

        $columnsQuery = DB::select("SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME='EziDebitTimestopHistory'");
        $columns = [];

        foreach ($columnsQuery as $column) {
            // Skip primary key
            if ($column->COLUMN_NAME === 'autoid') {
                continue;
            }

            $columns[] = [
                'name' => $column->COLUMN_NAME,
                'type' => $column->DATA_TYPE,
                'required' => $column->IS_NULLABLE === 'YES' ? 'false' : 'true'
            ];
        }
        
        usort($columns, function ($item1, $item2) {
            return $item1['name'] <=> $item2['name'];
        });

        echo '<pre>';
        foreach ($columns as $row) {
            echo '*      @OA\Parameter(
                *          name="' . $row['name'] . '",
                *          description="' . $row['name'] . '",
                *          required=' . strtolower($row['required']) . ',
                *          in="query",
                *          @OA\Schema(
                *              type="' . $sqlsrvDictionary[$row['type']]['type'] . '",
                *              format="' . $sqlsrvDictionary[$row['type']]['format'] . '"
                *          )
                *      ),
                ';
        }
        echo '</pre>';

        echo $this->fillable($columns);
    }

    public function fillable($columns)
    {
        return "'" . implode("', '", array_column($columns, 'name')) . "'";
        // implode(', ', array_column($columns, 'name'));
    }
}
