<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DepthDr extends Model
{
    public $table = 'depthdr';
    public $timestamps = false;
    
    protected $primaryKey = 'autoid';
    protected $fillable = ['UseExtendedAttributes', 'bpcxfile', 'butt', 'butt2', 'canc_from', 'canc_till', 'cancelled', 'cdesc', 'certno', 'code', 'crdate', 'crtime', 'date', 'dcode', 'depttype', 'dlevel', 'dow0', 'dow1', 'dow2', 'dow3', 'dow4', 'dow5', 'dow6', 'hidedept', 'node', 'rdesc', 'repflag1', 'repflag2', 'retain', 'sdate', 'snum', 'stime', 'time', 'ucode'];
}
