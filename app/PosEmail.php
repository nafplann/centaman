<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PosEmail extends Model
{
    public $table = 'POSEmail';
    public $timestamps = false;
    
    protected $primaryKey = 'autoid';
    protected $fillable = ['centernumber', 'emailaddress', 'emailsource', 'entityid', 'nodenumber', 'receiptamount', 'receiptdatetime', 'receiptnumber'];
}
