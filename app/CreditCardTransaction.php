<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CreditCardTransaction extends Model
{
    public $table = 'CreditCardTransactions';
    public $timestamps = false;
    
    protected $primaryKey = 'CCTID';
    protected $fillable = ['AccountType', 'AccountingDateTime', 'AcqRefData', 'AuthCode', 'AuthorizeAmount', 'CardType', 'CashOut', 'CmdStatus', 'CreditProcessor', 'CreditProcessorSubType', 'EntryMethod', 'InvoiceNo', 'MerchantID', 'NodeNo', 'OperatorID', 'ReceiptNo', 'ReceiptText', 'RecordNo', 'RefNo', 'RequestedPurchaseAmount', 'STAN', 'SignMaximumX', 'SignMaximumY', 'Signature', 'Source', 'TerminalID', 'TextResponse', 'TipAmount', 'TranCode', 'TransDateTime', 'TransNo'];
}
